/*
libndi.h
Copyright (C) 2020 VideoLAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef LIBNDI_LIBNDI_H
#define LIBNDI_LIBNDI_H

#include <libavutil/buffer.h>

typedef struct ndi_ctx ndi_ctx;

ndi_ctx *libndi_init(void);

enum ndi_data_type
{
    NDI_DATA_VIDEO,
    NDI_DATA_AUDIO,
    NDI_DATA_TEXT
};

#define NDI_NUM_DATA_POINTERS 16

typedef struct
{
    int data_type;

    /* Common */
    uint32_t fourcc;

    /* Video */
    uint8_t *data;
    int len;

    int fps_num;
    int fps_den;

    int width;
    int height;

    int picture_structure;

    /* Audio */
    AVBufferRef *buf[NDI_NUM_DATA_POINTERS];
    int samples;
    int num_channels;
    int sample_rate;
} ndi_data;

/* Discovery API */
typedef struct ndi_discovery_ctx ndi_discovery_ctx_t;
typedef struct ndi_discovery_item {
    char *name;
    char *ip;
    char *port;
} ndi_discovery_item_t;

static inline void libndi_discovery_item_destroy(ndi_discovery_item_t *item) {
    free(item->name);
    free(item->ip);
    free(item->port);
    free(item);
}

typedef void (*libndi_discovery_cb)(ndi_discovery_item_t *item);

ndi_discovery_ctx_t *libndi_discovery_init(libndi_discovery_cb callback);
int libndi_discovery_start(ndi_discovery_ctx_t *ctx);
int libndi_discovery_stop(ndi_discovery_ctx_t *ctx);
void libndi_discovery_destroy(ndi_discovery_ctx_t *ctx);

void libndi_list_sources(ndi_ctx *ndi_ctx);

typedef struct
{
    char *ip;
    char *port;
} ndi_opts;

int libndi_setup(ndi_ctx *ndi_ctx, ndi_opts *ndi_opts);

typedef void (*ndi_data_callback)(ndi_data *ndi_data);
void libndi_receive_data(ndi_ctx *ndi_ctx, ndi_data_callback callback);

void libndi_close(ndi_ctx *ndi_ctx);

#endif
